//settings
var tile_size = 20; //px

var start_map = {
    tiles: [
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,1,1,1,1,1],
        [0,0,0,1,1,0,0,0,0,0,2,0,0,0,0,0,0,1,0,0,0,0,0,1,1,1,1,1],
        [0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,2,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,2,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,2,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,2,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,2,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0],
        [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,3,0],
        [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0]
    ]
};

var tile_types = {
    0: { color: '#eeeeee', collide: false, effect: function(  ) {  } },
    1: { color: 'blue', collide: true, effect: function(  ) {  } },
    2: { color: 'red', collide: true, effect: function(  ) { health = health - 10; } },
    3: { color: 'green', collide: false, effect: function(  ) { end( true ); } },
    4: { color: 'red', collide: false, effect: function(  ) { health = health - 1; } }
};

var player_start = { x: 0, y: 0 };

var max_health = 100;

//globals
var looptime;
var width;
var height;
var canvas;
var context;
var map;
var health;
var health_timer;
var player_position;

function initialise(  ) {
    width = start_map.tiles[0].length;
    height = start_map.tiles.length;

    //set up canvas
    canvas = document.getElementById( 'game' );
    canvas.width = width * tile_size;
    canvas.height = height * tile_size;
    context = canvas.getContext( '2d' );

    tile_types[4].color = gradient(  );

    paintMenu( 'Welcome' );
}

function paintMenu( text ) {
    //paint the background and border
    context.fillStyle = '#eeeeee';
    context.fillRect( 0, 0, width * tile_size, height * tile_size );
    context.strokeStyle = 'black';
    context.strokeRect( 0, 0, width * tile_size, height * tile_size );

    context.fillStyle = "black";
    context.font = "bold 16px Arial";
    context.textAlign = 'center';
    context.fillText( text, Math.floor( ( width * tile_size ) / 2 ), Math.floor( ( width * tile_size ) / 2 ) - 15 );
    context.fillText( "Click Anywhere to Begin", Math.floor( ( width * tile_size ) / 2 ), Math.floor( ( width * tile_size ) / 2 ) + 15 );

    canvas.onclick = function(  ) { start(  ) };
}

function start(  ) {
    //set all globals
    health = max_health;
    health_timer = 0;
    map = start_map;

    //player start position
    player_position = player_start;

    //take off click event
    canvas.onclick = function(  ) {  };

    //set keypress event
    window.onkeydown = function( e ) { keyPress( e ) };

    //start loop
    loop(  );
}

function loop(  ) {
    looptime = setInterval( function(  ) {
        paint(  );

        //check health
        if ( health <= 0 ) {
            end( false );
        }

        //recharge health
        if ( health_timer % 50 == 0 ) {
            health++;
        }

        if ( health > max_health ) {
            health = max_health;
        }

        health_timer++;
    }, 10 );
}

function end( win ) {
    // end loop
    clearInterval( looptime );

    // paint menu
    if ( win ) {
        paintMenu( 'You Won' );
    } else {
        paintMenu( 'You Lost' );
    }

}

//Loop functions
function paint(  ) {
    //paint tile
    for ( var y = 0; y < map.tiles.length; y++ ) {
        for ( var x = 0; x < map.tiles[y].length; x++ ) {
            paintTile( x, y, tile_types[getTile( x, y )].color );
        }
    }

    //paint player
    paintPlayer(  );

    //paint health
    paintHealth(  );
}

function getTile( x, y ) {
    return map.tiles[y][x];
}

function paintTile( x, y, color ) {
    context.fillStyle = color;
    context.fillRect( x * tile_size, y * tile_size, tile_size, tile_size );
}

function paintPlayer(  ) {
    context.fillStyle = 'black';
    context.fillRect( player_position.x, player_position.y, tile_size, tile_size );
}

function paintHealth(  ) {
    context.fillStyle = "black";
    context.textAlign = 'right';
    context.fillText( 'Health: ' + health, width * tile_size - 5, 20 )
}

function move( dir ) {
    var current_x = player_position.x;
    var current_y = player_position.y;

    var new_x = current_x;
    var new_y = current_y;

    if ( dir == 'up' ) {
        new_y = current_y - 3;
    } else if ( dir == 'down' ) {
        new_y = current_y + 3;
    } else if ( dir == 'left' ) {
        new_x = current_x - 3;
    } else if ( dir == 'right' ) {
        new_x = current_x + 3;
    }

    if ( ! collisions( new_x, new_y ) ) {
        player_position.x = new_x;
        player_position.y = new_y;
    }
}

function collisions( x, y ) {
    var r_val = false;

    //find out which tiles are being touched
    collide_tiles = [];

    var corners = [
        { x: x, y: y },
        { x: x + tile_size, y: y },
        { x: x, y: y + tile_size },
        { x: x + tile_size, y: y + tile_size }
    ];

    //check each corner
    for ( var k = 0; k < corners.length; k++ ) {
        var x_tile = Math.floor( corners[k].x / tile_size );
        var y_tile = Math.floor( corners[k].y / tile_size );

        console.log( x_tile + ' - ' + y_tile );

        //do the effect
        tile_types[getTile( x_tile, y_tile )].effect(  );

        //if collidable object
        if ( tile_types[getTile( x_tile, y_tile )].collide == true ) {
            r_val = true;
        }
    }

    return r_val;
}

function keyPress( e ) {
    if ( e.which == '38' ) { //UP
        e.preventDefault(  );
        move( 'up' );
    }
    if ( e.which == '40' ) { //DOWN
        e.preventDefault(  );
        move( 'down' );
    }
    if ( e.which == '37' ) { //LEFT
        e.preventDefault(  );
        move( 'left' );
    }
    if ( e.which == '39' ) { //RIGHT
        e.preventDefault(  );
        move( 'right' );
    }
}

function gradient(  ) {
    var my_gradient = context.createLinearGradient( 0, 0, width * tile_size, 0 );
    my_gradient.addColorStop( 0, "white" );
    my_gradient.addColorStop( 0.5, "red" );
    my_gradient.addColorStop( 1, "white" );
    return my_gradient;
}

