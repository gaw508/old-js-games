function initialise( map_load ) {
    //settings
    var screen_height = window.innerHeight - 4;
    var screen_width = window.innerWidth - 4;

    var block_width = screen_height;
    var horizontal_view_load = 90;

    var floor_color_load = '#8A4B08';
    var sky_color_load = '#81DAF5';

    var tile_types_load = {
        0: { color: 'rgba(0,0,0,0.0)', collide: false, effect: function(  ) {  } },
        1: { color: 'blue', collide: true, effect: function(  ) {  } },
        2: { color: 'red', collide: true, effect: function(  ) { health = health - 10; } },
        3: { color: 'green', collide: false, effect: function(  ) { end( true ); } },
        4: { color: 'rgba(100,0,0,0.5)', collide: false, effect: function(  ) { health = health - 1; } },
        5: { color: 'black', collide: true, effect: function(  ) {  } }
    };
    var player_start = { x: 1.5 * block_width, y: 1.5 * block_width, a: 0 };
    //var player_start = { x: map_load.tiles.length / 2 * block_width, y: map_load.tiles[0].length / 2 * block_width, a: 0 };
    var max_health_load = 100;

    //intialise game
    width = screen_width;
    height = screen_height;

    initCanvas(  );

    start_map = map_load;
    max_health = max_health_load;
    tile_types = tile_types_load;
    horizontal_view = horizontal_view_load;
    initial_player_position = player_start;

    sky_color = sky_color_load;
    floor_color = floor_color_load;

    paintMenu( 'Welcome' );
}