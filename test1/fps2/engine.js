var loop_var;
var width;
var height;
var horizontal_view;
var start_map;
var canvas;
var context;
var map;
var tile_types;
var health;
var max_health;
var health_timer;
var initial_player_position;
var player_position;
var floor_color;
var sky_color;

var click_on = false;

function initCanvas(  ) {
    //set up canvas
    canvas = document.getElementById( 'game' );
    canvas.width = width;
    canvas.height = height;
    context = canvas.getContext( '2d' );
}

function paintMenu( text ) {
    //paint the background and border
    context.fillStyle = '#eeeeee';
    context.fillRect( 0, 0, width, height );
    context.strokeStyle = 'black';
    context.strokeRect( 0, 0, width, height );

    context.fillStyle = "black";
    context.font = "bold 16px Arial";
    context.textAlign = 'center';
    context.fillText( text, Math.floor( width / 2 ), Math.floor( height / 2 ) - 30 );
    context.fillText( "Get to the green block and avoid the reds", Math.floor( width / 2 ), Math.floor( height / 2 ) );
    context.fillText( "Click Anywhere to Begin", Math.floor( width / 2 ), Math.floor( height / 2 ) + 30 );

    canvas.onclick = function(  ) { start(  ) };
}

function start(  ) {
    health = max_health;
    health_timer = 0;
    map = start_map;
    player_position = initial_player_position;

    console.log( player_position );
    console.log( initial_player_position );

    window.onkeydown = function( e ) { keyPress( e ) };

    canvas.onclick = function(  ) { click_on = true };

    loop(  );
}

function end( win ) {
    // end loop
    clearInterval( loop_var );

    // paint menu
    if ( win ) {
        paintMenu( 'You Won' );
    } else {
        paintMenu( 'You Lost' );
    }
}

function getTile( x, y ) {
    return tile_types[map.tiles[y][x]];
}

function dist( a, b ) {
    return Math.sqrt( Math.pow( a, 2 ) + Math.pow( b, 2 ) );
}

function loop(  ) {
    loop_var = setInterval( function(  ) {
        paint(  );

        //check health
        if ( health <= 0 ) {
            end( false );
        }

        //recharge health
        if ( health_timer % 50 == 0 ) {
            health++;
        }

        if ( health > max_health ) {
            health = max_health;
        }

        health_timer++;
    }, 10 );
}

function paint(  ) {
    //blank
    context.fillStyle = sky_color;
    context.fillRect( 0, 0, width, height / 2 );
    context.fillStyle = floor_color;
    context.fillRect( 0, height / 2, width, height / 2 );

    context.strokeStyle = 'black';
    context.strokeRect( 0, 0, width, height );

    //find blocks within visible radius and field of vision
    //TODO: do this
    var blocks = [];
    for ( var y = 0; y < map.tiles.length; y++ ) {
        for ( var x = 0; x < map.tiles[0].length; x++ ) {
            if ( map.tiles[y][x] != 0 ) {
                blocks.push( { x: x * height, y: y * height, color: getTile( x, y ).color } );
            }
        }
    }

    //order blocks
    //work out closest corner for each block
    for ( var i = 0; i < blocks.length; i++ ) {
        var corns = [];
        corns.push( dist( blocks[i].x - player_position.x, blocks[i].y - player_position.y ) );
        corns.push( dist( blocks[i].x + height - player_position.x, blocks[i].y - player_position.y ) );
        corns.push( dist( blocks[i].x - player_position.x, blocks[i].y + height - player_position.y ) );
        corns.push( dist( blocks[i].x + height - player_position.x, blocks[i].y + height - player_position.y ) );

        var current = null;
        for ( var j = 0; j < corns.length; j++ ) {
            if ( current == null ) {
                current = corns[j];
            } else {
                if ( corns[j] < current ) {
                    current = corns[j];
                }
            }
        }

        blocks[i].closest = current;
    }

    //order blocks from furthest corner to closest
    //selection sort
    var k, l, kMax, hold;
    for ( k = 0; k < blocks.length; k++ ) {
        kMax = k;
        for ( l = k + 1; l < blocks.length; l++ ) {
            if ( blocks[l].closest > blocks[kMax].closest ) {
                kMax = l
            }
        }

        //swap
        if ( kMax != k ) {
            hold = blocks[k];
            blocks[k] = blocks[kMax];
            blocks[kMax] = hold;
        }
    }

    //draw blocks in new order
    var m;
    for ( m = 0; m < blocks.length; m++ ) {
        drawBlock( blocks[m] );
    }

    //paint health
    context.fillStyle = "black";
    context.textAlign = 'right';
    context.fillText( 'Health: ' + health, width - 5, 20 );
}

function drawBlock( block ) {
    var surfaces = getSurfaces( block );
    drawSurface( surfaces[0] );
    drawSurface( surfaces[1] );

    if ( click_on ) {
        click_on = false;
    }
}

function getSurfaces( block ) {
    //get closest corner type
    var corns1 = [];
    corns1.push( { dist: dist( block.x - player_position.x, block.y - player_position.y ), corn: 'tl' } );
    corns1.push( { dist: dist( block.x + 500 - player_position.x, block.y - player_position.y ), corn: 'tr' } );
    corns1.push( { dist: dist( block.x - player_position.x, block.y + 500 - player_position.y ), corn: 'bl' } );
    corns1.push( { dist: dist( block.x + 500 - player_position.x, block.y + 500 - player_position.y ), corn: 'br' } );

    var current2 = { dist: null };
    for ( var j = 0; j < corns1.length; j++ ) {
        if ( current2.dist == null ) {
            current2 = corns1[j];
        } else {
            if ( corns1[j].dist < current2.dist ) {
                current2 = corns1[j];
            }
        }
    }

    var sur1 = {
        points: [
            { x: block.x, y: block.y, z: height / 2 },
            { x: block.x, y: block.y, z: - height / 2 },
            { x: block.x, y: block.y, z: height / 2 },
            { x: block.x, y: block.y, z: - height / 2 }
        ],
        color: block.color
    };
    var sur2 = {
        points: [
            { x: block.x, y: block.y, z: height / 2 },
            { x: block.x, y: block.y, z: - height / 2 },
            { x: block.x, y: block.y, z: height / 2 },
            { x: block.x, y: block.y, z: - height / 2 }
        ],
        color: block.color
    };

    var tl_x = 0;
    var tl_y = 0;
    var tr_x = height;
    var tr_y = 0;
    var bl_x = 0;
    var bl_y = height;
    var br_x = height;
    var br_y = height;

    if ( current2.corn == 'tl' ) {
        //tl/tr surface, line1 is tl, line2 is tr
        sur1.points[0].x = sur1.points[0].x + tl_x;
        sur1.points[1].x = sur1.points[1].x + tl_x;
        sur1.points[2].x = sur1.points[2].x + tr_x;
        sur1.points[3].x = sur1.points[3].x + tr_x;
        sur1.points[0].y = sur1.points[0].y + tl_y;
        sur1.points[1].y = sur1.points[1].y + tl_y;
        sur1.points[2].y = sur1.points[2].y + tr_y;
        sur1.points[3].y = sur1.points[3].y + tr_y;

        //tl/bl surface, line1 is tl, line2 is bl
        sur2.points[0].x = sur2.points[0].x + tl_x;
        sur2.points[1].x = sur2.points[1].x + tl_x;
        sur2.points[2].x = sur2.points[2].x + bl_x;
        sur2.points[3].x = sur2.points[3].x + bl_x;
        sur2.points[0].y = sur2.points[0].y + tl_y;
        sur2.points[1].y = sur2.points[1].y + tl_y;
        sur2.points[2].y = sur2.points[2].y + bl_y;
        sur2.points[3].y = sur2.points[3].y + bl_y;

    } else if ( current2.corn == 'tr' ) {
        //tr/tl
        sur1.points[0].x = sur1.points[0].x + tr_x;
        sur1.points[1].x = sur1.points[1].x + tr_x;
        sur1.points[2].x = sur1.points[2].x + tl_x;
        sur1.points[3].x = sur1.points[3].x + tl_x;
        sur1.points[0].y = sur1.points[0].y + tr_y;
        sur1.points[1].y = sur1.points[1].y + tr_y;
        sur1.points[2].y = sur1.points[2].y + tl_y;
        sur1.points[3].y = sur1.points[3].y + tl_y;

        //tr/br
        sur2.points[0].x = sur2.points[0].x + tr_x;
        sur2.points[1].x = sur2.points[1].x + tr_x;
        sur2.points[2].x = sur2.points[2].x + br_x;
        sur2.points[3].x = sur2.points[3].x + br_x;
        sur2.points[0].y = sur2.points[0].y + tr_y;
        sur2.points[1].y = sur2.points[1].y + tr_y;
        sur2.points[2].y = sur2.points[2].y + br_y;
        sur2.points[3].y = sur2.points[3].y + br_y;
    } else if ( current2.corn == 'bl' ) {
        //bl/tl
        sur1.points[0].x = sur1.points[0].x + bl_x;
        sur1.points[1].x = sur1.points[1].x + bl_x;
        sur1.points[2].x = sur1.points[2].x + tl_x;
        sur1.points[3].x = sur1.points[3].x + tl_x;
        sur1.points[0].y = sur1.points[0].y + bl_y;
        sur1.points[1].y = sur1.points[1].y + bl_y;
        sur1.points[2].y = sur1.points[2].y + tl_y;
        sur1.points[3].y = sur1.points[3].y + tl_y;

        //bl/br
        sur2.points[0].x = sur2.points[0].x + bl_x;
        sur2.points[1].x = sur2.points[1].x + bl_x;
        sur2.points[2].x = sur2.points[2].x + br_x;
        sur2.points[3].x = sur2.points[3].x + br_x;
        sur2.points[0].y = sur2.points[0].y + bl_y;
        sur2.points[1].y = sur2.points[1].y + bl_y;
        sur2.points[2].y = sur2.points[2].y + br_y;
        sur2.points[3].y = sur2.points[3].y + br_y;
    } else if ( current2.corn == 'br' ) {
        //br/tr
        sur1.points[0].x = sur1.points[0].x + br_x;
        sur1.points[1].x = sur1.points[1].x + br_x;
        sur1.points[2].x = sur1.points[2].x + tr_x;
        sur1.points[3].x = sur1.points[3].x + tr_x;
        sur1.points[0].y = sur1.points[0].y + br_y;
        sur1.points[1].y = sur1.points[1].y + br_y;
        sur1.points[2].y = sur1.points[2].y + tr_y;
        sur1.points[3].y = sur1.points[3].y + tr_y;

        //br/bl
        sur2.points[0].x = sur2.points[0].x + br_x;
        sur2.points[1].x = sur2.points[1].x + br_x;
        sur2.points[2].x = sur2.points[2].x + bl_x;
        sur2.points[3].x = sur2.points[3].x + bl_x;
        sur2.points[0].y = sur2.points[0].y + br_y;
        sur2.points[1].y = sur2.points[1].y + br_y;
        sur2.points[2].y = sur2.points[2].y + bl_y;
        sur2.points[3].y = sur2.points[3].y + bl_y;
    }

    return [ sur1, sur2 ];
}

function getAngle( x, y ) {
    dx = x - player_position.x;
    dy = y - player_position.y;
    return Math.atan2( dx, dy ) * ( 180 / Math.PI );
}

function drawSurface( sur ) {
    context.fillStyle = sur.color;

    startAngle = player_position.a - ( horizontal_view / 2 );
    endAngle = player_position.a + ( horizontal_view / 2 );

    //draw 0-1-3-2
    //turn xyz into xy flat
    //angle from left (screen x)

    vertical_view = ( height / width ) * horizontal_view;

    var i, a, flat_dist, z, theta;
    for ( i = 0; i < sur.points.length; i++ ) {
        //get x
        a = getAngle( sur.points[i].x, sur.points[i].y );
        diff = player_position.a - a;
        if ( diff > 180 ) {
            diff = diff - 360;
        }
        sur.points[i].screenHAngle = ( width / 2 ) + ( ( diff / horizontal_view ) * width );

        //get y
        flat_dist = dist( sur.points[i].x - player_position.x, sur.points[i].y - player_position.y );
        z = sur.points[i].z;
        theta = Math.atan2( z , flat_dist ) * ( 180 / Math.PI );
        sur.points[i].screenVAngle = ( ( ( vertical_view / 2 ) - theta ) / vertical_view ) * height;
    }

    if ( click_on ) {
        console.log( sur );
    }

    if ( outsideView( sur.points[0] ) && outsideView( sur.points[1] ) && outsideView( sur.points[2] ) && outsideView( sur.points[3] ) ) {

    } else {
        context.beginPath(  );
        context.moveTo( sur.points[0].screenHAngle, sur.points[0].screenVAngle );
        context.lineTo( sur.points[1].screenHAngle, sur.points[1].screenVAngle );
        context.lineTo( sur.points[3].screenHAngle, sur.points[3].screenVAngle );
        context.lineTo( sur.points[2].screenHAngle, sur.points[2].screenVAngle );
        context.closePath(  );
        context.fill(  );
    }

    //console.log( sur );

    //end( );
}

function outsideView( point ) {
    if ( point.screenHAngle < 0 || point.screenHAngle > width ) {
        return true;
    } else {
        return false;
    }
}

function move( dir ) {
    var current_x = player_position.x;
    var current_y = player_position.y;

    var new_x = current_x;
    var new_y = current_y;

    //get maginitude of each
    var mag_x = Math.sin( player_position.a * ( Math.PI / 180 ) ) * 60;
    var mag_y = Math.cos( player_position.a * ( Math.PI / 180 ) ) * 60;

    if ( dir == 'up' ) {
        new_y = current_y + mag_y;
        new_x = current_x + mag_x;
    } else if ( dir == 'down' ) {
        new_y = current_y - mag_y;
        new_x = current_x - mag_x;
    } else if ( dir == 'left' ) {
        new_y = current_y - mag_x;
        new_x = current_x + mag_y;
    } else if ( dir == 'right' ) {
        new_y = current_y + mag_x;
        new_x = current_x - mag_y;
    }

    if ( ! moveCollisions( new_x, new_y ) ) {
        player_position.x = new_x;
        player_position.y = new_y;
    }

    console.log( 'player: x: ' + player_position.x + ' y: ' + player_position.y + ' a: ' + player_position.a );
}

function turn( dir ) {
    if ( dir == 'left' ) {
        player_position.a = player_position.a + 5;
    } else if ( dir == 'right' ) {
        player_position.a = player_position.a - 5;
    }

    if ( player_position.a < 0 ) {
        player_position.a = 360 + player_position.a;
    }

    if ( player_position.a >= 360 ) {
        player_position.a = player_position.a - 360;
    }
}

function shoot(  ) {

}

function moveCollisions( x, y ) {
    var r_val = false;

    //find out which tiles are being touched
    collide_tiles = [];

    var x_tile = Math.floor( x / height );
    var y_tile = Math.floor( y / height );

    //do the effect
    getTile( x_tile, y_tile ).effect(  );

    //if collidable object
    if ( getTile( x_tile, y_tile ).collide == true ) {
        r_val = true;
    }

    return r_val;
}

function keyPress( e ) {
    if ( e.which == '38' ) { //UP 38
        e.preventDefault(  );
        move( 'up' );
    }
    if ( e.which == '40' ) { //DOWN 40
        e.preventDefault(  );
        move( 'down' );
    }
    if ( e.which == '37' ) { //LEFT 37
        e.preventDefault(  );
        move( 'left' );
    }
    if ( e.which == '39' ) { //RIGHT 39
        e.preventDefault(  );
        move( 'right' );
    }

    if ( e.which == '90' ) { //Z 90
        e.preventDefault(  );
        turn( 'left' );
    }
    if ( e.which == '88' ) { //X 88
        e.preventDefault(  );
        turn( 'right' );
    }

    if ( e.which == '32' ) { //SPACE 32
        e.preventDefault(  );
        shoot(  );
    }
}

