function initialise( map_load ) {
    //settings
    var screen_width = 500;
    var horizontal_view_load = 45;
    var vertical_view_load = 45;
    var tile_types_load = {
        0: { color: 'rgba(0,0,0,0.0)', collide: false, effect: function(  ) {  } },
        1: { color: 'blue', collide: true, effect: function(  ) {  } },
        2: { color: 'red', collide: true, effect: function(  ) { health = health - 10; } },
        3: { color: 'green', collide: false, effect: function(  ) { end( true ); } },
        4: { color: 'yellow', collide: false, effect: function(  ) { health = health - 1; } }
    };
    var player_start = { x: 1000, y: 1000, a: 70 };
    var max_health_load = 100;

    //intialise game
    width = screen_width;
    height = ( vertical_view_load / horizontal_view_load ) * screen_width;
    initCanvas(  );

    start_map = map_load;
    max_health = max_health_load;
    tile_types = tile_types_load;
    horizontal_view = horizontal_view_load;
    vertical_view = vertical_view_load;
    player_position = player_start;

    paintMenu( 'Welcome' );
}