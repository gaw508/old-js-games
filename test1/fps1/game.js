var loop_var;
var width;
var height;
var horizontal_view;
var vertical_view;
var start_map;
var canvas;
var context;
var map;
var tile_types;
var health;
var max_health;
var health_timer;
var player_position;

function initCanvas(  ) {
    //set up canvas
    canvas = document.getElementById( 'game' );
    canvas.width = width;
    canvas.height = height;
    context = canvas.getContext( '2d' );
}

function paintMenu( text ) {
    //paint the background and border
    context.fillStyle = '#eeeeee';
    context.fillRect( 0, 0, width, height );
    context.strokeStyle = 'black';
    context.strokeRect( 0, 0, width, height );

    context.fillStyle = "black";
    context.font = "bold 16px Arial";
    context.textAlign = 'center';
    context.fillText( text, Math.floor( width / 2 ), Math.floor( width / 2 ) - 15 );
    context.fillText( "Click Anywhere to Begin", Math.floor( width / 2 ), Math.floor( width / 2 ) + 15 );

    canvas.onclick = function(  ) { start(  ) };
}

function start(  ) {
    health = max_health;
    health_timer = 0;
    map = start_map;

    window.onkeydown = function( e ) { keyPress( e ) };

    loop(  );
}

function end( win ) {
    // end loop
    clearInterval( loop_var );

    // paint menu
    if ( win ) {
        paintMenu( 'You Won' );
    } else {
        paintMenu( 'You Lost' );
    }
}

function loop(  ) {
    loop_var = setInterval( function(  ) {
        paint(  );
    }, 10 );
}

function getTile( x, y ) {
    return tile_types[map.tiles[y][x]];
}

function dist( a, b ) {
    return Math.sqrt( Math.pow( a, 2 ) + Math.pow( b, 2 ) );
}

function paint(  ) {
    //blank
    context.fillStyle = '#eeeeee';
    context.fillRect( 0, 0, width, height );
    context.strokeStyle = 'black';
    context.strokeRect( 0, 0, width, height );

    //find blocks within visible radius and field of vision
    //TODO: do this
    var blocks = [];
    for ( var y = 0; y < map.tiles.length; y++ ) {
        for ( var x = 0; x < map.tiles[0].length; x++ ) {
            if ( map.tiles[y][x] != 0 ) {
                blocks.push( { x: x * 500, y: y * 500, color: getTile( x, y ).color } );
            }
        }
    }

    //work out closest corner for each block
    for ( var i = 0; i < blocks.length; i++ ) {
        var corns = [];
        corns.push( dist( blocks[i].x - player_position.x, blocks[i].y - player_position.y ) );
        corns.push( dist( blocks[i].x + 500 - player_position.x, blocks[i].y - player_position.y ) );
        corns.push( dist( blocks[i].x - player_position.x, blocks[i].y + 500 - player_position.y ) );
        corns.push( dist( blocks[i].x + 500 - player_position.x, blocks[i].y + 500 - player_position.y ) );

        var current = null;
        for ( var j = 0; j < corns.length; j++ ) {
            if ( current == null ) {
                current = corns[j];
            } else {
                if ( corns[j] < current ) {
                    current = corns[j];
                }
            }
        }

        blocks[i].closest = current;
    }

    //order blocks from furthest corner to closest
    for ( var z = 1; z < blocks.length; z++ ) {
        for ( k = i; k > 0 && blocks[k] < blocks[k-1]; k-- ) {
            var old_k = blocks[k];
            blocks[k] = blocks[k-1];
            blocks[k-1] = old_k;
        }
    }

    //for each block, draw the two surfaces coming off the closest corner
    var surfaces = [];
    for ( var i = 0; i < blocks.length; i++ ) {
        var corns1 = [];
        corns1.push( { dist: dist( blocks[i].x - player_position.x, blocks[i].y - player_position.y ), corn: 'tl' } );
        corns1.push( { dist: dist( blocks[i].x + 500 - player_position.x, blocks[i].y - player_position.y ), corn: 'tr' } );
        corns1.push( { dist: dist( blocks[i].x - player_position.x, blocks[i].y + 500 - player_position.y ), corn: 'bl' } );
        corns1.push( { dist: dist( blocks[i].x + 500 - player_position.x, blocks[i].y + 500 - player_position.y ), corn: 'br' } );

        var current2 = { dist: null };
        for ( var j = 0; j < corns1.length; j++ ) {
            if ( current2.dist == null ) {
                current2 = corns1[j];
            } else {
                if ( corns1[j].dist < current2.dist ) {
                    current2 = corns1[j];
                }
            }
        }

        var sur1 = {
            line1: { x: blocks[i].x, y: blocks[i].y, dist: 0, angle: 0 },
            line2: { x: blocks[i].x, y: blocks[i].y, dist: 0, angle: 0 },
            color: blocks[i].color
        };
        var sur2 = {
            line1: { x: blocks[i].x, y: blocks[i].y, dist: 0, angle: 0 },
            line2: { x: blocks[i].x, y: blocks[i].y, dist: 0, angle: 0 },
            color: blocks[i].color
        };

        if ( current2.corn == 'tl' ) {
            //tl/tr surface, line1 is tl, line2 is tr
            sur1.line2.x = sur1.line2.x + 500;

            //tl/bl surface, line1 is tl, line2 is bl
            sur2.line2.y = sur2.line2.y + 500;

        } else if ( current2.corn == 'tr' ) {
            //tr/tl
            sur1.line1.x = sur1.line1.x + 500;

            //tr/br
            sur2.line1.x = sur2.line1.x + 500;
            sur2.line2.x = sur2.line2.x + 500;
            sur2.line2.y = sur2.line2.y + 500;
        } else if ( current2.corn == 'bl' ) {
            //bl/tl
            sur1.line1.y = sur1.line1.y + 500;

            //bl/br
            sur2.line1.y = sur2.line1.y + 500;
            sur2.line2.x = sur2.line2.x + 500;
            sur2.line2.y = sur2.line2.y + 500;
        } else if ( current2.corn == 'br' ) {
            //br/tr
            sur1.line1.x = sur1.line1.x + 500;
            sur1.line1.y = sur1.line1.y + 500;
            sur1.line2.x = sur1.line2.x + 500;

            //br/bl
            sur2.line1.x = sur2.line1.x + 500;
            sur2.line1.y = sur2.line1.y + 500;
            sur2.line2.y = sur2.line2.y + 500;
        }

        sur1.line1.dist = dist( sur1.line1.x - player_position.x, sur1.line1.y - player_position.y );
        sur1.line2.dist = dist( sur1.line2.x - player_position.x, sur1.line2.y - player_position.y );

        sur2.line1.dist = dist( sur2.line1.x - player_position.x, sur2.line1.y - player_position.y );
        sur2.line2.dist = dist( sur2.line2.x - player_position.x, sur2.line2.y - player_position.y );

        sur1.line1.angle = getAngle( sur1.line1.x, sur1.line1.y );
        sur1.line2.angle = getAngle( sur1.line2.x, sur1.line2.y );

        sur2.line1.angle = getAngle( sur2.line1.x, sur2.line1.y );
        sur2.line2.angle = getAngle( sur2.line2.x, sur2.line2.y );

        surfaces.push( { d1: sur1.line1.dist, a1: sur1.line1.angle, d2: sur1.line2.dist, a2: sur1.line2.angle, color: sur1.color } );
        surfaces.push( { d1: sur2.line1.dist, a1: sur2.line1.angle, d2: sur2.line2.dist, a2: sur2.line2.angle, color: sur2.color } );
    }

    //paint the surfaces
    for ( var m = 0; m < surfaces.length; m++ ) {
        paintSurface( surfaces[m].d1, surfaces[m].a1, surfaces[m].d2, surfaces[m].a2, surfaces[m].color );
    }
    //end(true);
}

function getAngle( x, y ) {
    dx = x - player_position.x;
    dy = y - player_position.y;
    return Math.atan2( dx, dy ) * 180 / Math.PI;
}

function paintSurface( d1, a1, d2, a2, color ) {
    startAngle = player_position.a - 30;
    endAngle = player_position.a + 30;

    console.log( a1 );

    vertex1 = {
        distance: d1/ 10,
        angle: ( ( a1 - startAngle ) / 60 ) * 500
    };

    vertex2 = {
        distance: d2 / 10,
        angle: ( ( a2 - startAngle ) / 60 ) * 500
    };

    context.fillStyle = color;
    context.beginPath();
    context.moveTo(vertex1.angle, vertex1.distance );
    context.lineTo(vertex1.angle, height - vertex1.distance );
    context.lineTo(vertex2.angle, height - vertex2.distance );
    context.lineTo(vertex2.angle, vertex2.distance );
    context.closePath();
    context.fill();
}

function move( dir ) {
    var current_x = player_position.x;
    var current_y = player_position.y;

    var new_x = current_x;
    var new_y = current_y;

    if ( dir == 'up' ) {
        new_y = current_y - 3;
    } else if ( dir == 'down' ) {
        new_y = current_y + 3;
    } else if ( dir == 'left' ) {
        new_x = current_x - 3;
    } else if ( dir == 'right' ) {
        new_x = current_x + 3;
    }

    player_position.x = new_x;
    player_position.y = new_y;
}

function turn( dir ) {
    if ( dir == 'left' ) {
        player_position.a = player_position.a - 5;
    } else if ( dir == 'right' ) {
        player_position.a = player_position.a + 5;
    }
}

function keyPress( e ) {
    if ( e.which == '38' ) { //UP 38
        e.preventDefault(  );
        move( 'right' );
    }
    if ( e.which == '40' ) { //DOWN 40
        e.preventDefault(  );
        move( 'left' );
    }
    if ( e.which == '37' ) { //LEFT 37
        e.preventDefault(  );
        move( 'down' );
    }
    if ( e.which == '39' ) { //RIGHT 39
        e.preventDefault(  );
        move( 'up' );
    }

    if ( e.which == '49' ) {
        e.preventDefault(  );
        turn( 'left' );
    }

    if ( e.which == '50' ) {
        e.preventDefault(  );
        turn( 'right' );
    }
}